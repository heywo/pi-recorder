### pi-recorder readme ###
**pi-recorder** is a small application that will allow you to turn your [Raspberry Pi](https://www.raspberrypi.org) into a small recording device. Simply attach a microphone as an audio input to your Pi, configure **pi-recorder**, and you can remotely control your recording studio through a web interface. 

Users can start and stop recording through the web interface and **pi-recorder** will automatically upload your recordings securely via SSH to a server of your choice!

The objective of **pi-recorder** is to replace expensive recording equipment for simple voice recordings in settings such as churches or conferences.

## installation instructions ##
1. Make sure **npm** and **node** are installed on your machine
2. Clone the repository
3. Copy the **/config/default.json** file and rename it **/config/dev.json** and update the variables accordingly
4. Run **npm install** in the root directory
5. From the root directory run **npm start**
6. Navigate to **http://localhost:3000** to view the webpage

## troubleshooting ##
A log file is output to the **tmp** folder along with the temporary recordings (until they are moved to the **recordings** folder)

## roadmap ##
* Configure recording directories
* Output log file to web UI
* Simplified installation/run process
* **pi-recorder** running as a service full time
* Move sshpass to use a password file versus environment variable

## history ##
* **2017.02.25** 
* * Simplified installation/run process
* **2017.02.15** 
* * Initial release version 0.1