var path = require('path');
var config = require("config");
var util = require('util'),
    exec = require('child_process').exec,
    child;

exports.post = function(req, res) {

    var action = req.body.action;

    var errorHandler = function(error, stdout, stderr) {
        if (error !== null)
             console.log('exec error: ' + error);
    }

    if (action === 1)
    {	
        console.log('starting recording');
        
        process.env.AUDIODEV = config.get("pi-recorder.SERVER_AUDIODEV");
        process.env.AUDIODRIVER = config.get("pi-recorder.SERVER_AUDIODRIVER");
        var env = Object.create(process.env);
        
        child = exec(path.resolve(__dirname + '/../scripts/static_record.sh'), {env: env}, errorHandler);
    }
    else 
    {
        console.log('stopping recording');
        process.env.SERVER_IP = config.get("pi-recorder.SERVER_IP");
        process.env.SERVER_USERNAME = config.get("pi-recorder.SERVER_USERNAME");
        process.env.SSHPASS = config.get("pi-recorder.SERVER_PASSWORD");
        process.env.SERVER_DEST_FOLDER = config.get("pi-recorder.SERVER_DEST_FOLDER");
        var env = Object.create(process.env);
		child = exec(path.resolve(__dirname + '/../scripts/recording_stop.sh'), { env: env }, errorHandler);
    }
    
    var response = {
        status: 200
    };

    res.end(JSON.stringify(response));
}
