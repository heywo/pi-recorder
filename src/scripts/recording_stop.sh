#!/bin/bash
echo "stopping recording"

kill $(cat /tmp/static_record.pid) || echo "Process is not running."

DATE=$(date +%Y%m%d%H%M)
LOGDATE=$(date +"%Y-%m-%d %H:%M:%S")

mv tmp/recording.wav recordings/$DATE.wav

echo "[$LOGDATE] Successfully recorded $DATE.wav" >> tmp/log.txt

sshpass -e scp -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -P 2222 recordings/$DATE.wav $SERVER_USERNAME@$SERVER_IP:$SERVER_DEST_FOLDER

if [ $? -eq 0 ]; then
	echo "[$LOGDATE] Successfully uploaded $DATE.wav" >> tmp/log.txt
else
	echo "[$LOGDATE] Failed to upload $DATE.wav to the server"
fi
