#!/bin/bash

cmd="rec tmp/recording.wav"
nohup $cmd &

bg_pid=$!

mkdir -p /tmp
mkdir -p /recordings

echo $bg_pid >> /tmp/static_record.pid

echo $bg_pid

wait $bg_pid

rm /tmp/static_record.pid
