(function() {

    $(document).ready(function() {

        var recordingSeconds = 0;
        var timer = null;

        var recordingTimer = function() {
            recordingSeconds++;
            $("#recordingTimer").html(recordingSeconds + " seconds");
        };

        $("#test").click(function() {

            var routeType = parseInt($(this).data("action"));
            var route = "recording";
            var data = {
                action: 0
            };

            if (routeType === 0)
            {
                $(this).data("action", 1);
                $(this).html("Stop Recording");
                data.action = 1;
            }
            else
            {
                $(this).data("action", 0);
                $(this).html("Start Recording");
                data.action = 0;
            }    

            var success = function() {
				
				recordingSeconds = 0;
				
                if (routeType === 0)
                {
                    timer = setInterval(recordingTimer, 1000);
                    $("#recordingTimer").html(recordingSeconds + " seconds");
                    $("#recordingTimer").show();
                }
                else
                {
                    $("#recordingTimer").hide();
                    clearInterval(timer);
                }
            }

            var failure = function() {
                console.log("oops");
            }

            pir.postData(route, data, success, failure, failure);
        });

    })
})();
