var pir = pir || {};

pir.IsUndefinedNullOrEmpty = function(o) {
    if (o === undefined || o === null || o === "") {
        return true;
    }
    return false;
}

pir.postData = function(url, data, success, failure, error) {
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(data),
            processData: false,
            contentType: 'application/json'
        })
        .done(function(data) {
            if (data.Error) {
                if (error) {
                    error(data);
                } else {
                    console.log(data.ErrorMessage);
                }
            } else {
                if (success) {
                    success(data);
                }
            }
        })
        .fail(function(jqXhr, status, error) {
            if (pir.IsUndefinedNullOrEmpty(error))
            {
                error = "Undefined.";
            }

            var errorMessage = "A network error occured. (" + error + ")";

            if (failure)
            {
                failure(errorMessage);
            }
        });
};