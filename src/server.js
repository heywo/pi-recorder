var version = "0.1";

var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");

var mainController = require("./controllers/main");
var recordingController = require("./controllers/recording");

var app = express();
var port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

var router = express.Router();

router.route("/").get(mainController.get);

router.route("/recording").post(recordingController.post);

console.log(__dirname + '/../node_modules/jquery/dist/jquery.min.js');

app.use(express.static(__dirname + "/public"));
app.use('/jquery', express.static(__dirname + '/../node_modules/jquery/dist/'));
app.use(router);
app.listen(port);

console.log("pi-recorder " + version + " (" + process.env.NODE_ENV +") started on port " + port);
